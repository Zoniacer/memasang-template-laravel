@extends('adminlte.master')

@section('content')
<div class="card card-primary mt-3 mx-3">
    <div class="card-header">
        <h3 class="card-title">Edit Cast {{ $cast->id }}</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
    <form action="/cast/{{ $cast->id }}" method="POST">
        @csrf
        @method("PUT")
        <div class="card-body">
            <div class="form-group">
                <label for="nama">Nama</label>
                <input type="text" class="form-control" id="nama" name="nama" value="{{ old('nama', $cast->nama) }}"
                    placeholder="Masukkan nama">
                @error('nama')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="umur">Umur</label>
                <input type="number" class="form-control" id="umur" name="umur" value="{{ old('umur', $cast->umur) }}"
                    placeholder="Masukkan umur">
                @error('umur')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="bio">Bio</label>
                <input type="text" class="form-control" id="bio" name="bio" value="{{ old('bio', $cast->bio) }}"
                    placeholder="Masukkan bio">
                @error('bio')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
        </div>
        <!-- /.card-body -->

        <div class="card-footer">
            <button type="submit" class="btn btn-primary">Edit</button>
        </div>
    </form>
</div>
@endsection
